package fr.developpix.neural_network_java.neurons;

/**
 * Class representing a neuron using the sigmoid function
 * 
 * @author developpix
 * @version 0.1
 */
public class SigmoidNeuron extends Neuron {

	/**
	 * Constructor to create a neuron with a number of inputs
	 * 
	 * @param nbInputs the number of inputs
	 */
	public SigmoidNeuron(int nbInputs) {
		super(nbInputs);
	}

	@Override
	public double getOutput() {
		double sum = 0.0;

		// Calculate the sum of inputs
		for (int i = 0; i < this.inputs.length; i++) {
			sum += this.inputs[i] * this.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Return the result of the sigmoid function
		return 1 / denominator;
	}

}
