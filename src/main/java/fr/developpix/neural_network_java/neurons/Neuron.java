package fr.developpix.neural_network_java.neurons;

import fr.developpix.neural_network_java.exceptions.InvalidInputsException;

/**
 * Class representing a neuron
 * 
 * @author developpix
 * @version 0.1
 */
public abstract class Neuron {

	protected double[] inputs;
	protected double[] weights;
	protected double error;

	/**
	 * Constructor
	 * 
	 * @param nbInputs number of neuron's inputs
	 */
	public Neuron(int nbInputs) {
		this.inputs = new double[nbInputs];
		this.weights = new double[nbInputs];
		this.error = 0.0;
		
		// Initialize weights with random value
		for(int i = 0; i < nbInputs; i++) {
			this.weights[i] = Math.random();
		}
	}

	/**
	 * 
	 * @param newInputs the array of new inputs
	 * 
	 * @throws InvalidInputsException exception throws if the new inputs was invalid
	 */
	public void setInputs(double[] newInputs) throws InvalidInputsException {
		// If there is too much input's values
		if (newInputs.length > this.inputs.length)
			throw new InvalidInputsException("Neuron received two many inputs");

		this.inputs = newInputs;
	}
	
	/**
	 * Method to get the neuron's error value
	 * 
	 * @return the neuron's error value
	 */
	public double getError() {
		return this.error;
	}
	
	/**
	 * Method to set a new neuron's error value
	 * 
	 * @param newError the new neuron's error value
	 */
	public void setError(double newError) {
		this.error = newError;
	}

	/**
	 * Method calculate the output of the neuron based on the inputs values
	 * 
	 * @return the output of the neuron
	 */
	public abstract double getOutput();
}
