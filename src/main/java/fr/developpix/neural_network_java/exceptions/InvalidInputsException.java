package fr.developpix.neural_network_java.exceptions;

public class InvalidInputsException extends Exception {

	public InvalidInputsException() {
		super("Invalid inputs");
	}

	public InvalidInputsException(String message) {
		super(message);
	}

	public InvalidInputsException(Throwable cause) {
		super(cause);
	}

	public InvalidInputsException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidInputsException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
