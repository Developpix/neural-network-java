package fr.developpix.neural_network_java.neurons;

import org.junit.Before;
import org.junit.Test;

import fr.developpix.neural_network_java.exceptions.InvalidInputsException;
import junit.framework.TestCase;

public class SigmoidNeuronTest extends TestCase {

	private Neuron neuron;

	@Before
	public void setUp() {
		this.neuron = new SigmoidNeuron(4);
	}

	@Test
	public void testOutputCT1() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { 0.0, 0.0, 0.0, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0", expected, this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT2() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { Double.MAX_VALUE, 0.0, 0.0, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted one set to max double value", expected, this.neuron.getOutput(),
				0.1E-15);
	}

	@Test
	public void testOutputCT3() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { Double.MAX_VALUE, 0.0, Double.MAX_VALUE, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted 2 set to max double value", expected, this.neuron.getOutput(),
				0.1E-15);
	}

	@Test
	public void testOutputCT4() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { Double.MAX_EXPONENT, 0.0, 0.0, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted one set to max exponent double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT5() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { Double.MAX_EXPONENT, 0.0, Double.MAX_EXPONENT, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted 2 set to max exponent double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT6() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { Double.MIN_VALUE, 0.0, 0.0, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted one set to min double value", expected, this.neuron.getOutput(),
				0.1E-15);
	}

	@Test
	public void testOutputCT7() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { Double.MIN_VALUE, 0.0, Double.MIN_VALUE, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted 2 set to min double value", expected, this.neuron.getOutput(),
				0.1E-15);
	}

	@Test
	public void testOutputCT8() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { Double.MIN_EXPONENT, 0.0, 0.0, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted one set to min exponent double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT9() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { Double.MIN_EXPONENT, 0.0, Double.MIN_EXPONENT, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted 2 set to min exponent double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT10() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { Double.MAX_VALUE, 0.0, 0.0, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted one set to negative max double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT11() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { -Double.MAX_VALUE, 0.0, -Double.MAX_VALUE, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted 2 set to negative max double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT12() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { -Double.MAX_EXPONENT, 0.0, 0.0, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted one set to negative max exponent double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT13() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { -Double.MAX_EXPONENT, 0.0, -Double.MAX_EXPONENT, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted 2 set to negative max exponent double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT14() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { Double.MIN_VALUE, 0.0, 0.0, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted one set to negative min double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT15() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { -Double.MIN_VALUE, 0.0, -Double.MIN_VALUE, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted 2 set to negative min double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT16() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { -Double.MIN_EXPONENT, 0.0, 0.0, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted one set to negative min exponent double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}

	@Test
	public void testOutputCT17() throws InvalidInputsException {
		this.neuron.setInputs(new double[] { -Double.MIN_EXPONENT, 0.0, -Double.MIN_EXPONENT, 0.0 });

		double sum = 0.0;
		// Calculate the sum of inputs
		for (int i = 0; i < this.neuron.inputs.length; i++) {
			sum += this.neuron.inputs[i] * this.neuron.weights[i];
		}

		// Calculate the denominator
		double denominator = 1 + Math.exp(-sum);

		// Calculate the expected result of the sigmoid function
		double expected = 1 / denominator;

		assertEquals("Test all inputs set to 0 excepted 2 set to negative min exponent double value", expected,
				this.neuron.getOutput(), 0.1E-15);
	}
}
